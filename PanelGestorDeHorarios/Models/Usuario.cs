﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PanelGestorDeHorarios.Models
{
    public class Usuario
    {
        [DisplayName("Folio")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string folio { get; set; } = null!;

        [DisplayName("Contraseña")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string password { get; set; } = null!;

        [DisplayName("Email")]
        public string email { get; set; } = null!;

        [DisplayName("Rol")]
        public string rol { get; set; } = null!;
    }
}
