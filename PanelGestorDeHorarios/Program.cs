using Microsoft.AspNetCore.Authentication.Cookies;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();


// Configuraciones para manejar autenticaci�n de usuarios.
// NOTA: no confundir con sesiones de usuario, ya que las sesiones son cookies que almacenan informaci�n personalizada, y eso no se aplicar� en este proyecto.
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options => {
        options.LoginPath = "/Home/Login"; // �En qu� p�gina se har� el login?
        options.ExpireTimeSpan = TimeSpan.FromMinutes(10); // �Cu�nto tiempo durar� la autenticaci�n?
        options.AccessDeniedPath = "/Shared/AccesoDenegado"; // Si el usuario autenticado no tiene roles o permisos para ver ciertas p�ginas, �A qu� p�gina se le redirigir�?
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}


app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication(); // Esto permite el uso de las autenticaciones en la aplicaci�n de ASP.NET Core MVC

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Login}/{id?}");

app.Run();
