﻿using Microsoft.AspNetCore.Mvc;

namespace PanelGestorDeHorarios.Controllers
{
    public class SharedController : Controller
    {
        public IActionResult AccesoDenegado()
        {
            return View();
        }
    }
}
