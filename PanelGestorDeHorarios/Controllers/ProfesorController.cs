﻿using Microsoft.AspNetCore.Mvc;
using PanelGestorDeHorarios.Models;
using System.Diagnostics;

namespace PanelGestorDeHorarios.Controllers
{
    public class ProfesorController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public ProfesorController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Perfil()
        {
            return View();
        }

        public IActionResult MiHorario()
        {
            return View();
        }
    }
}
