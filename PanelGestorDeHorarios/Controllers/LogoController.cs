﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.IO;
using System.Threading.Tasks;

namespace TuProyecto.Controllers
{
    public class LogoController : Controller
    {
        public IActionResult UploadLogo()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile imageFile)
        {
            if (imageFile != null && imageFile.Length > 0)
            {
                // Guardar la imagen en el servidor
                var imagePath = Path.Combine("wwwroot", "logo.png");
                using (var stream = new FileStream(imagePath, FileMode.Create))
                {
                    await imageFile.CopyToAsync(stream);
                }

                // Redirigir o realizar alguna otra acción después de la carga exitosa
                return RedirectToAction("Index", "Home");
            }
            else
            {
                // Manejar el caso en que no se seleccionó ningún archivo
                ModelState.AddModelError("", "Por favor selecciona una imagen.");
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
