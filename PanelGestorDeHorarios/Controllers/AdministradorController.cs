﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using System.IO;

namespace PanelGestorDeHorarios.Controllers
{
    public class AdministradorController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public AdministradorController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GenerarHorarios()
        {
            return View();
        }

        public IActionResult ConsultarHorarios()
        {
            return View();
        }

        public IActionResult ConsultarPerfilProfesor()
        {
            return View();
        }

		public IActionResult PanelAdministrador()
		{
			return View();
		}

        [HttpGet]
        public async Task<IActionResult> GenerarHorariosConsumir()
        {
            string apiURL = "https://oip33he60m.execute-api.us-east-2.amazonaws.com/horario/generado";
                
            using (HttpClient client = new())
            {
                try
                {
                    // Realizar una solicitud GET al endpoint de la API
                    HttpResponseMessage response = await client.GetAsync(apiURL);
                    Console.WriteLine(apiURL);

                    // Verificar si la solicitud fue exitosa
                    if (response.IsSuccessStatusCode)
                    {
                        // Leer el contenido de la respuesta como una cadena JSON
                        string responseBody = await response.Content.ReadAsStringAsync();

                        Console.WriteLine("Respuesta de la API:");
                        Console.WriteLine(responseBody);
                        TempData["StringHorarioGenerado"] = responseBody;

                        GenerateExcelFromJson("horarios.xlsx", responseBody);
                    }
                    else
                    {
                        Console.WriteLine($"La solicitud no fue exitosa. Código de estado: {response.StatusCode}");
                        TempData["ErrorAlGenerarHorario"] = "No se ha podido generar un horario. Intente de nuevo más tarde.";
                    }
                    return RedirectToAction("GenerarHorarios", "administrador");
                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine($"Error al realizar la solicitud HTTP: {ex.Message}");
                    TempData["ErrorAlGenerarHorario"] = "No se ha podido generar un horario. Intente de nuevo más tarde.";
                    return RedirectToAction("GenerarHorarios", "administrador");
                }
            }
        }

        public void GenerateExcelFromJson(string excelFilePath, string jsonText)
        {
            // Parsear el JSON a un objeto dinámico
            dynamic jsonData = JObject.Parse(jsonText);

            // Crear un nuevo archivo Excel
            using (ExcelPackage excelPackage = new ExcelPackage())
            {
                // Agregar una hoja al libro de trabajo
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.Add("Data");

                // Escribir los datos en el archivo Excel
                int rowIndex = 1;
                foreach (var item in jsonData)
                {
                    int colIndex = 1;
                    foreach (var prop in item)
                    {
                        worksheet.Cells[rowIndex, colIndex].Value = prop.ToString();
                        colIndex++;
                    }
                    rowIndex++;
                }

                // Guardar el archivo Excel en disco
                FileInfo excelFile = new FileInfo(excelFilePath);
                excelPackage.SaveAs(excelFile);
            }
        }

    }
}
