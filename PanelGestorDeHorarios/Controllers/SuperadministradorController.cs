﻿using Microsoft.AspNetCore.Mvc;
using PanelGestorDeHorarios.Models;
using System.Diagnostics;

namespace PanelGestorDeHorarios.Controllers
{
    public class SuperadministradorController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public SuperadministradorController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult PersonalizarPagina()
        {
            return View();
        }

        public IActionResult Soporte()
        {
            return View();
        }

        public IActionResult PanelSuperadministrador()
        {
            return View();
        }
    }
}
