﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using PanelGestorDeHorarios.Models;
using System.Text.Json;

namespace PanelGestorDeHorarios.Controllers
{
    public class RolesController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Login(Usuario datosLogin)
        {
            Console.WriteLine(datosLogin.folio);
            Console.WriteLine(datosLogin.password);

            string rol = "";
            string baseUrl = "https://d5stm6fym6.execute-api.us-east-2.amazonaws.com/login/usuario";
            string queryString = $"?folio={datosLogin.folio}&password={datosLogin.password}";

            // Crear una instancia de HttpClient
            using (HttpClient client = new())
            {
                try
                {
                    // Realizar una solicitud GET al endpoint de la API
                    HttpResponseMessage response = await client.GetAsync(baseUrl + queryString);
                    Console.WriteLine(baseUrl + queryString);

                    // Verificar si la solicitud fue exitosa
                    if (response.IsSuccessStatusCode)
                    {
                        // Leer el contenido de la respuesta como una cadena JSON
                        string responseBody = await response.Content.ReadAsStringAsync();

                        Console.WriteLine("Respuesta de la API:");
                        Console.WriteLine(responseBody);

                        // Crear root, que es una representación del JSON en un objeto de C#
                        var doc = JsonDocument.Parse(responseBody);
                        var root = doc.RootElement;

                        var folio = root.GetProperty("folio").GetString();
                        rol = root.GetProperty("rol").GetString() ?? "";
                        Console.WriteLine($"{folio} {rol}");

                        //    Console.WriteLine($"{folio} {rol}");
                        //// Como se garantiza que la respuesta de esta API es un Array JSON, entonces se recorre cada objeto de ese array.
                        //foreach (var user in root.EnumerateArray())
                        //{
                        //    var folio = user.GetProperty("folio").GetString();
                        //    rol = user.GetProperty("rol").GetString();
                        //    Console.WriteLine($"{folio} {rol}");
                        //}
                    }
                    else
                    {
                        Console.WriteLine($"La solicitud no fue exitosa. Código de estado: {response.StatusCode}");
                    }
                }
                catch (HttpRequestException ex)
                {
                    Console.WriteLine($"Error al realizar la solicitud HTTP: {ex.Message}");
                }
            }

            // Si encuentra un usuario y contraseña válidos en un mismo registro, entonces se crea una cookie de autenticación en la aplicación.
            if (rol != "")
            {
                // Se crea una lista de propiedades (Claims) para la autenticación. En este caso, se toma el nombre del usuario del formulario y se le asignan roles. Los roles servirán para saber qué páginas puede o no visitar el usuario.
                var claims = new List<Claim> {
                    // new(ClaimTypes.Name, autenticacion.id_usuario.ToString()),
                    new(ClaimTypes.Name, "Alan"),
                    // new(ClaimTypes.Role, autenticacion.id_rol.ToString()) // Se puede asignar una lista de roles al usuario autenticado si se toman de una BD.
                    new(ClaimTypes.Role, rol ?? "")
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                // Autenticar al usuario con los Claims especificados.
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToAction("RedireccionarSegunRol", "AsignarVista");
            }
            else
            { // De no haber ingresado un usuario y contraseña válidos, recargará la página, mostrando un mensaje de que has introducido un usuario o contraseña inválidos.
                TempData["MensajeDeErrorLogin"] = "Usuario o contraseña inválidos. Intente de nuevo"; // TempData persiste una redirección, cosa que ni ViewBag ni ViewData pueden hacer.
                return RedirectToAction("Login", "Home");
            }
        }

        public async Task<IActionResult> LoginSuper(Usuario autenticacion)
        {
            string rol = "superadministrador";

            // Si encuentra un usuario y contraseña válidos en un mismo registro, entonces se crea una cookie de autenticación en la aplicación.
            if (true)
            {
                // Se crea una lista de propiedades (Claims) para la autenticación. En este caso, se toma el nombre del usuario del formulario y se le asignan roles. Los roles servirán para saber qué páginas puede o no visitar el usuario.
                var claims = new List<Claim> {
                    // new(ClaimTypes.Name, autenticacion.id_usuario.ToString()),
                    new(ClaimTypes.Name, "Alan"),
                    // new(ClaimTypes.Role, autenticacion.id_rol.ToString()) // Se puede asignar una lista de roles al usuario autenticado si se toman de una BD.
                    new(ClaimTypes.Role, rol)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                // Autenticar al usuario con los Claims especificados.
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToAction("RedireccionarSegunRol", "AsignarVista");
            }
            else
            { // De no haber ingresado un usuario y contraseña válidos, recargará la página, mostrando un mensaje de que has introducido un usuario o contraseña inválidos.
                TempData["MensajeDeErrorLogin"] = "Usuario o contraseña inválidos. Intente de nuevo"; // TempData persiste una redirección, cosa que ni ViewBag ni ViewData pueden hacer.
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> LoginAdmin(Usuario autenticacion)
        {
            string rol = "administrador";

            // Si encuentra un usuario y contraseña válidos en un mismo registro, entonces se crea una cookie de autenticación en la aplicación.
            if (true)
            {
                // Se crea una lista de propiedades (Claims) para la autenticación. En este caso, se toma el nombre del usuario del formulario y se le asignan roles. Los roles servirán para saber qué páginas puede o no visitar el usuario.
                var claims = new List<Claim> {
                    // new(ClaimTypes.Name, autenticacion.id_usuario.ToString()),
                    new(ClaimTypes.Name, "Alan"),
                    // new(ClaimTypes.Role, autenticacion.id_rol.ToString()) // Se puede asignar una lista de roles al usuario autenticado si se toman de una BD.
                    new(ClaimTypes.Role, rol)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                // Autenticar al usuario con los Claims especificados.
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToAction("RedireccionarSegunRol", "AsignarVista");
            }
            else
            { // De no haber ingresado un usuario y contraseña válidos, recargará la página, mostrando un mensaje de que has introducido un usuario o contraseña inválidos.
                TempData["MensajeDeErrorLogin"] = "Usuario o contraseña inválidos. Intente de nuevo"; // TempData persiste una redirección, cosa que ni ViewBag ni ViewData pueden hacer.
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> LoginProfesor(Usuario autenticacion)
        {
            string rol = "profesor";

            // Si encuentra un usuario y contraseña válidos en un mismo registro, entonces se crea una cookie de autenticación en la aplicación.
            if (true)
            {
                // Se crea una lista de propiedades (Claims) para la autenticación. En este caso, se toma el nombre del usuario del formulario y se le asignan roles. Los roles servirán para saber qué páginas puede o no visitar el usuario.
                var claims = new List<Claim> {
                    // new(ClaimTypes.Name, autenticacion.id_usuario.ToString()),
                    new(ClaimTypes.Name, "Alan"),
                    // new(ClaimTypes.Role, autenticacion.id_rol.ToString()) // Se puede asignar una lista de roles al usuario autenticado si se toman de una BD.
                    new(ClaimTypes.Role, rol)
                };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                // Autenticar al usuario con los Claims especificados.
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                return RedirectToAction("RedireccionarSegunRol", "AsignarVista");
            }
            else
            { // De no haber ingresado un usuario y contraseña válidos, recargará la página, mostrando un mensaje de que has introducido un usuario o contraseña inválidos.
                TempData["MensajeDeErrorLogin"] = "Usuario o contraseña inválidos. Intente de nuevo"; // TempData persiste una redirección, cosa que ni ViewBag ni ViewData pueden hacer.
                return RedirectToAction("Index");
            }
        }

        public async Task<IActionResult> Logout()
        {
            // 
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme); // El usuario autenticado será desautenticado, por lo que ya no podrá ver páginas que requieran autenticación.
            return RedirectToAction("Login", "Home");
        }
    }
}
