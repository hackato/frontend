﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PanelGestorDeHorarios.Controllers
{
    public class AsignarVistaController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }

        [Authorize(Roles = "superadministrador")]
        public IActionResult Superadministrador()
        {
            return View();
        }

        [Authorize(Roles = "administrador")]
        public IActionResult Administrador()
        {
            return View();
        }

        [Authorize(Roles = "profesor")]
        public IActionResult Profesor()
        {
            return View();
        }

        public IActionResult RedireccionarSegunRol()
        {
            if (User.IsInRole("superadministrador"))
            {
                return RedirectToAction("index", "superadministrador");
            }
            else if (User.IsInRole("administrador"))
            {
                return RedirectToAction("index", "administrador");
            }
            else if (User.IsInRole("profesor"))
            {
                return RedirectToAction("index", "profesor");
            }
            else
            {
                // Manejo para otros roles o sin rol
                return RedirectToAction("Login", "Home");
            }
        }
    }
}
